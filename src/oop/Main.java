package oop;

import oop.Apple;

public class Main {
    public static void main(String[] args) {
        Apple apple1 = new Apple();
        System.out.println(apple1.getColour() + " " + apple1.getSize());
        Apple apple2 = new Apple("red", 3);
        System.out.println(apple2.getColour() + " " + apple2.getSize());
        apple1.rot(5, "black");
        System.out.println(apple1.getColour() + " " + apple1.getSize());
        apple1.setSize(2);
        System.out.println(apple1.getColour() + " " + apple1.getSize());
    }
}
