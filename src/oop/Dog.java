package oop;

public class Dog {
    private String name;
    private int age;

    public Dog(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Dog() {
        name = "";
        age = 0;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void barkByDefault() {
        System.out.println("Woof-woof-woof-woof");
    }

    public String barkByAge(int age) {

        if (age < 3) {
            return "Woof-woof";}
        else if (age > 3 && age < 10){
            return "Classy barking";}
        else {
            return "Too old for barking";
        }
    }
}
