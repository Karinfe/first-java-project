package oop;

public class Room {
    private String name;
    private int width; // private int float width
    private int height;
    private int length;

    public void Room(String name, int width, int height, int length) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getLength() {
        return length;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setLength(int length) {
        this.length = length;
    }
    public int surfaceArea(int width, int height) {
        return (width * height);
    }
    public int volume(int width, int height, int length) {
        return (width * height * length);
    }
}
// you can also use "float" to see decimals.