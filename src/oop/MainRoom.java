package oop;

import java.util.Scanner;

public class MainRoom {
    public static void main(String[] args) {
        Room livingRoom = new Room();

        System.out.println("Please enter the width of your living room in meters");
        Scanner inputWidth = new Scanner(System.in);
        livingRoom.setWidth(inputWidth.nextInt());

        System.out.println("Please enter the length of your living room in meters");
        Scanner inputLength = new Scanner(System.in);
        livingRoom.setLength(inputLength.nextInt());

        System.out.println("Please enter the height of your living room in meters");
        Scanner inputHeight = new Scanner(System.in);
        livingRoom.setHeight(inputHeight.nextInt());
        System.out.println("Your living room area is " + livingRoom.surfaceArea(livingRoom.getWidth(), livingRoom.getLength()) + "m2");
        System.out.println("Your living room volume is " + livingRoom.volume(livingRoom.getWidth(), livingRoom.getLength(), livingRoom.getHeight()) + "m3");
    }
}
// you can also use "float" to see decimals. Also you an add room name also to a System Print.
// Room room = new Room();
// System.out.println("Please enter name for the room")
