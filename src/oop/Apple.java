package oop;

public class Apple {
    private String colour;
    private int size;

    public Apple(String colour, int size) {
        this.colour = colour;
        this.size = size;
    }
        public Apple(){
            colour ="";
            size = 0;
        }

    public String rot(int rotTime, String rotColour) {
        while (rotTime > 0) {
            System.out.println("Rotting...");
            --rotTime;
        }
        colour = rotColour;
        return  "The apple is rotten.";
    }

    public int getSize() {
        return size;
    }

    public void setColour() {
        this.colour = colour;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getColour() {
        return colour;
    }
}
