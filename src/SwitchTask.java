import java.util.Scanner;

public class SwitchTask {
    public static void main(String[] args) {
        System.out.println("Please enter a number");
        Scanner weekday = new Scanner(System.in);
        int day = weekday.nextInt();
        switch (day){
            case 0:
                System.out.println("?");
                break;
            case 1:
                System.out.println("monday");
                break;
            case 6:
            case 7:
                System.out.println("weekend");
                break;
            case 5:
                System.out.println("friyay!");
                break;
            default:
                System.out.println("work work work");
        }
    }
}
