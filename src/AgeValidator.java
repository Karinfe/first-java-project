import java.util.Scanner;

public class AgeValidator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Please enter your age:");
        int age = input.nextInt();
        if (age > 0 && age < 18){
            System.out.println("minor");
        }
        else if (age >= 18 && age < 65) {
            System.out.println("adult");
        }
        else if (age >= 65) {
            System.out.println("senior");
        }
        else {
            System.out.println("incorrect age");
        }
    }

}
