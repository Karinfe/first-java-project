import javax.swing.plaf.synth.SynthTextAreaUI;
import java.util.Scanner;

public class ProgramTask {
    public static void main(String[] args) {
        System.out.println("Please enter a number from 0-9");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        if ((number > 0) || (number < 9)){
            System.out.println("You entered the number " + number + " and doing quite good");
        }
        else if (number == 3){
            System.out.println("This is your lucky number 3");
        }
        else if (number == 5){
            System.out.println("This is your lucky number 5");
        }
        else {
            System.out.println("Too many numbers");
        }
        }
    }
