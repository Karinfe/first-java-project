import java.lang.reflect.Array;
import java.util.ArrayList;

public class ArrayExampleSimple {
    public static void main(String[] args) {
        ArrayList<Integer> pairs = new ArrayList<>();
        int[] arrayInt = new int[]{-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};
        int sum = 3;
        for (int i = 0; i < arrayInt.length; i++) {
            for (int j = 0; j < arrayInt.length; j++) {
                if (j != i
                        && (arrayInt[i] + arrayInt[j]) == sum
                        && !pairs.contains(arrayInt[i])
                        && !pairs.contains(arrayInt[j])) {
                    pairs.add(arrayInt[i]);
                    pairs.add(arrayInt[j]);
                }

            }
        }
        for (int i = 0; i < pairs.size(); i += 2) {
            System.out.println(pairs.get(i) + ":" + pairs.get(i + 1));
        }
    }
}
