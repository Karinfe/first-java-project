public class ArrayLoop {
    public static void main(String[] args) {
        //here we have an array of numbers. The task: print all the pairs that have a sum of 3
        int[] arrayOfIntegers = {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};
        // create the sum 3 as a variable to use
        int sum = 3;
        /*
        for (statement 1 ; statement 2 ; statement 3 ) {
        code to be executed };
        statement 1 - executed once before the execution of the code block
        statement 2 - defines the CONDITION for executing the code block
        statement 3 - is executed every time AFTER the code block has been executed
        */
        for (int i = 0; i < arrayOfIntegers.length; i++) {
            /* initialize the counter at 0 (first place in array),
            counter is smaller than the length of the array, after each code block, add 1 to i
             */
            for (int j = 0; j < arrayOfIntegers.length; j++) {
                /*
                to get a sum, we need two numbers. initialize j to the first place in array,
                j is also smaller than the array's length,
                after each code block, add 1 to j
                 */
                if ( j != i && (arrayOfIntegers[i] + arrayOfIntegers[j]) == sum) {
                    /*
                    IF j does NOT equal i AND the numbers corresponding
                    to i's and j's current places in the array
                    equal sum, then...
                     */
                    System.out.println(arrayOfIntegers[i] + ":" + (sum - arrayOfIntegers[i]));
                    /*
                    ... print the number in i's position, print : and
                    print sum - the number in i's position
                     */
                    //CODE BLOCK ENDS HERE, RE-LOOP
                }
            }
        }
    }
}
